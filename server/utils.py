import jwt

secret_key = 'top-secret-toptal-s3cr3t-signing'

def generate_token(id, role):
    return jwt.encode({'id': id, 'role': role}, secret_key, algorithm='HS256')
