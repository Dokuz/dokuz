import json
import math
import uuid
from datetime import datetime

import bcrypt
import motor.motor_asyncio
from aiohttp import web
from aiohttp_middlewares import cors_middleware, error_middleware
from bson import json_util
from pymongo import ReturnDocument
import os

from middleware import check_auth, check_admin_auth
from utils import generate_token

client = motor.motor_asyncio.AsyncIOMotorClient()
db = client["toptal"]
users_coll = db["users"]
rest_coll = db["restaurants"]


async def create_user(request):
    req_json = await request.json()
    exists = await users_coll.count_documents({"email": req_json["email"]})
    if exists > 0:
        return web.json_response({'Status': 'failed', 'error': 'Email already exists'}, status=409)
    # TODO gelen fieldlari kontrol et, eksik fazla olmasin
    user_id = uuid.uuid4().hex

    if request.path != '/admin/user':
        req_json['role'] = 'USER'
    jwt_token = generate_token(user_id, req_json['role'])
    new_user = {**req_json, 'id': user_id, 'role': req_json['role'],
                'password': bcrypt.hashpw(req_json['password'].encode(), bcrypt.gensalt()),
                "token": jwt_token}
    await users_coll.insert_one(new_user)
    return web.json_response({'Status': 'Success', 'result': 'User Created'}, status=201)


async def login(request):
    req_json = await request.json()
    user = await users_coll.find_one({'email': req_json['email']})
    if user:
        matched = bcrypt.checkpw(req_json['password'].encode(), user['password'])
        if matched:
            return web.json_response({'Status': 'Success', 'result': {'token': user['token'], 'role': user['role']}},
                                     status=200)

    return web.Response(text=json.dumps({'status': 'failed', 'error': 'Wrong credentials'}), status=401)


async def edit_user(request):
    req_json = await request.json()
    user = await users_coll.find_one({'id': req_json['user_id']})
    if user:
        if 'fullname' in req_json:
            user['fullname'] = req_json['fullname']
        if 'role' in req_json:
            user['role'] = req_json['role']
        if 'username' in req_json:
            user['username'] = req_json['username']
        if 'password' in req_json:
            user['password'] = bcrypt.hashpw(req_json['password'].encode(), bcrypt.gensalt())
        if 'email' in req_json:
            user['email'] = req_json['email']

        updated_user = await users_coll.find_one_and_update({'id': req_json['user_id']}, {"$set": user},
                                                            {'_id': 0, 'password': 0, 'token': 0},
                                                            return_document=ReturnDocument.AFTER)
        return web.json_response({'Status': 'Success', 'result': {'user': updated_user}}, status=200)
    return web.json_response(text=json.dumps({'status': 'failed', 'result': 'User not found'}), status=404)


async def get_user(request):
    user_id = request.match_info.get("id", None)
    if user_id:
        result = await users_coll.find_one({'id': user_id}, {'_id': 0, 'password': 0, 'token': 0})
        if result:
            return web.json_response(text=json.dumps(result), status=200)
        else:
            return web.json_response({'status': 'failed', 'reason': 'User not found'}, status=404)

    page = int(request.rel_url.query['page']) if 'page' in request.rel_url.query else 1
    page_size = 10
    total_count = await users_coll.count_documents({})
    page_count = math.ceil(float(total_count) / page_size)
    result = users_coll.find({}, {'_id': 0, 'password': 0, 'token': 0}).skip(page_size * (page - 1)).limit(
        page_size).sort('_id', -1)
    result = await result.to_list(None)
    return web.json_response({'users': result, 'page_count': page_count}, status=200)


async def delete_user(request):
    user_id = request.match_info.get("id", None)
    result = await users_coll.find_one_and_delete({'id': user_id})
    if result:
        return web.Response(text=json.dumps({'status': 'success'}), status=200)
    return web.json_response({'status': 'failed', 'reason': 'user not found'}, status=404)


async def create_restaurant(request):
    req_json = await request.json()
    # TODO gelen fieldlari kontrol et, eksik fazla olmasin
    new_restaurant = {**req_json, 'id': uuid.uuid4().hex, 'reviews': []}
    await rest_coll.insert_one(new_restaurant)
    return web.json_response({'status': 'success', 'result': 'Restaurant created'}, status=201)


async def edit_restaurant(request):
    req_json = await request.json()
    restaurant = await rest_coll.find_one({'id': req_json['id']}, {'_id': 0})
    if restaurant is None:
        return web.Response(text="Restaurant not found!", status=404)
    # TODO gelen fieldlari kontrol et, eksik fazla olmasin
    updated_restaurant = await rest_coll.find_one_and_update({'id': req_json['id']},
                                                             {"$set": {**restaurant, **req_json}}, {'_id': 0},
                                                             return_document=ReturnDocument.AFTER)
    return web.json_response({'Status': 'Success', 'result': {'user': updated_restaurant}}, status=200)


async def delete_restaurant(request):
    restaurant_id = request.match_info.get("id", None)
    restaurant = await rest_coll.find_one({'id': restaurant_id})
    if restaurant is None:
        return web.Response(text=json.dumps({'status': 'failed', 'reason': 'Restaurant not found'}), status=404)

    result = await rest_coll.find_one_and_delete({'id': restaurant_id})
    if result:
        return web.Response(text=json.dumps({'status': 'success'}), status=200)
    return web.Response(text=json.dumps({'status': 'failed', 'reason': 'user not found'}), status=404)


async def get_restaurant(request):
    restaurant_id = request.match_info.get("id", None)
    page = int(request.rel_url.query['page']) if 'page' in request.rel_url.query else 1
    if restaurant_id:
        restaurant = await rest_coll.find_one({'id': restaurant_id}, {'_id': 0})
        if restaurant is None:
            return web.json_response(text=json.dumps({'status': 'failed', 'reason': 'Restaurant not found'}),
                                     status=404)

        reviews = restaurant['reviews']
        if len(reviews) == 0:
            return web.json_response(text=json.dumps(restaurant), status=200)

        if len(reviews) > 1:
            restaurant['max_rate_review'] = max(reviews, key=lambda r: str(r['stars']) + str(r['date']))
            restaurant['min_rate_review'] = min(reviews, key=lambda r: str(r['stars']) + str(r['date']))

        restaurant['latest_rev'] = max(reviews, key=lambda r: r['date'])
        restaurant['avg_rate'] = sum(r['stars'] for r in reviews) / len(reviews)
        return web.json_response(text=json.dumps(restaurant), status=200)
    else:
        page_size = 10
        restaurants = await rest_coll.aggregate(
            [{"$set": {"avg_rate": {"$avg": "$reviews.stars"}}}, {"$sort": {"avg_rate": -1.0}},
             {"$skip": page_size * (page - 1)},
             {"$limit": page_size}, {"$project": {"_id": 0}}]).to_list(None)
        total_count = await rest_coll.count_documents({})
        page_count = math.ceil(float(total_count) / page_size)
        return web.json_response(json.loads(json_util.dumps({"restaurants": restaurants,
                                                             "page_count": page_count})), status=200)


async def add_review(request):
    reviewer_user = await users_coll.find_one({'id': request['user_id']})

    req_json = await request.json()
    review = req_json['review']
    restaurant_id = req_json['restaurant_id']

    restaurant = await rest_coll.find_one({'id': restaurant_id})
    if restaurant is None:
        return web.Response(text=json.dumps({'status': 'Invalid Restaurant Name!'}), status=403)

    review['date'] = datetime.today().timestamp()
    review['username'] = reviewer_user['username']
    review['id'] = uuid.uuid4().hex
    restaurant['reviews'].append(review)

    # TODO check success
    await rest_coll.update_one({'id': restaurant_id}, {"$set": restaurant})

    return web.json_response({"status": "success"}, status=200)


async def delete_review(request):
    req_json = await request.json()
    review_id = req_json['review_id']
    restaurant_id = req_json['restaurant_id']

    restaurant = await rest_coll.find_one({'id': restaurant_id})
    if restaurant is None:
        return web.Response(text=json.dumps({'status': 'failed', 'reason': 'Restaurant not found'}), status=404)

    ids = list(map(lambda r: r['id'], restaurant['reviews']))
    if review_id not in ids:
        return web.Response(text=json.dumps({'status': 'failed', 'reason': 'Review not found'}), status=404)

    restaurant['reviews'] = list(filter(lambda review: review['id'] != review_id, restaurant['reviews']))

    updated_restaurant = await rest_coll.find_one_and_update({'id': restaurant_id}, {"$set": restaurant},
                                                             {'_id': 0},
                                                             return_document=ReturnDocument.AFTER)
    return web.json_response(json.loads(json_util.dumps(updated_restaurant)), status=200)


async def edit_review(request):
    req_json = await request.json()
    review = req_json['review']
    restaurant_id = req_json['restaurant_id']
    review_id = review['id']

    restaurant = await rest_coll.find_one({'id': restaurant_id})
    if restaurant is None:
        return web.Response(text=json.dumps({'status': 'failed', 'reason': 'Restaurant not found'}), status=404)

    old_review = list(filter(lambda x: x['id'] != review_id, restaurant['reviews']))[0]
    restaurant['reviews'] = [*filter(lambda x: x['id'] != review_id, restaurant['reviews']), {**old_review, **review}]

    updated_restaurant = await rest_coll.find_one_and_update({'id': restaurant_id}, {"$set": restaurant},
                                                             {'_id': 0},
                                                             return_document=ReturnDocument.AFTER)
    return web.json_response(json.loads(json_util.dumps(updated_restaurant)), status=200)


async def get_review(request):
    restaurant_id = request.match_info.get("id", None)
    page = int(request.rel_url.query['page']) if 'page' in request.rel_url.query else 1
    page_size = 10
    offset = page_size * (page - 1)

    count_result = await rest_coll.aggregate(
        [{"$match": {'id': restaurant_id}}, {"$project": {"count": {"$size": "$reviews"}}}]).to_list(1)
    if len(count_result) > 0:
        total_count = count_result[0]['count']
        restaurant = await rest_coll.find_one({'id': restaurant_id},
                                              {'_id': 0, "reviews": {"$slice": [offset, offset + page_size]}})
        page_count = math.ceil(float(total_count) / page_size)
        page_count = 1 if page_count == 0 else page_count
        return web.json_response({'reviews': restaurant['reviews'][::-1], 'page_count': page_count}, status=200)
    else:
        return web.json_response({'reviews': [], 'page_count': 0}, status=200)


async def make_app():
    app = web.Application(middlewares=[
        cors_middleware(allow_all=True, allow_credentials=True), check_auth,
        # error_middleware()
    ])

    admin = web.Application(middlewares=[check_admin_auth])
    admin.router.add_post('/user', create_user)
    admin.router.add_patch('/user', edit_user)
    admin.router.add_delete('/user/{id}', delete_user)
    admin.router.add_get('/user', get_user)
    admin.router.add_get('/user/{id}', get_user)
    admin.router.add_post('/restaurant', create_restaurant)
    admin.router.add_patch('/restaurant', edit_restaurant)
    admin.router.add_delete('/restaurant/{id}', delete_restaurant)
    admin.router.add_patch('/review', edit_review)
    admin.router.add_delete('/review', delete_review)
    admin.router.add_get('/restaurant/{id}/review', get_review)

    app.add_subapp('/admin/', admin)

    app.router.add_post('/auth/login', login)
    app.router.add_post('/auth/register', create_user)

    app.router.add_get('/restaurant', get_restaurant)
    app.router.add_get('/restaurant/{id}', get_restaurant)
    app.router.add_post('/review', add_review)

    app['users_coll'] = users_coll
    app.on_startup.append(create_default_admin)

    return app


async def create_default_admin(app):
    user_count = await users_coll.count_documents({})
    if user_count == 0:
        user_id = uuid.uuid4().hex
        jwt_token = generate_token(user_id, 'ADMIN')
        admin_password = os.getenv("ADMIN_PASSWORD") or 'password'
        new_user = {'username': 'admin', 'id': user_id, 'role': 'ADMIN', 'email': 'admin@admin.com',
                    'password': bcrypt.hashpw(admin_password.encode(), bcrypt.gensalt()),
                    "token": jwt_token}
        await users_coll.insert_one(new_user)


web.run_app(make_app(), port=9000)
