import jwt
from aiohttp.web_exceptions import HTTPUnauthorized
from aiohttp.web_middlewares import middleware

from utils import secret_key


def decode_token(request):
    if request.headers.get('Authorization') is None:
        raise HTTPUnauthorized()

    headers = request.headers
    try:
        bearer_token = headers["Authorization"].split(" ")[1]
        decoded = jwt.decode(bearer_token, key=secret_key, algorithms=['HS256'])
        request['role'] = decoded['role']
        request['user_id'] = decoded['id']
        return decoded
    except:
        raise HTTPUnauthorized()


@middleware
async def check_auth(request, handler):
    if request.path.startswith('/auth'):
        return await handler(request)

    decode_token(request)
    return await handler(request)


@middleware
async def check_admin_auth(request, handler):
    decode_token(request)
    if request['role'] != 'ADMIN':
        raise HTTPUnauthorized()
    return await handler(request)
