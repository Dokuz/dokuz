import {Link} from "react-router-dom";
import Confirmation from "../Confirmation";
import {useState} from "react";
import ReactStars from "react-rating-stars-component";
import Button from "@mui/material/Button";

const RestaurantCard = ({name, id, token, deleteCallBack, averageRate, role}) => {
    const [show, setShow] = useState(false);

    const handleDelete = () => {
        fetch('http://localhost:9000/admin/restaurant/' + id, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            },
        }).then(() => {
            deleteCallBack()
            // history.push('/');
        })
    }

    const childToParent = (childData) => {
        if (childData) {
            handleDelete()

        }
        setShow(false);
    }


    return (
        <div className={"res-list"}>
            <div style={{padding: "20"}}>
                <div className="res-preview" key={id}>
                    <Link to={`/restaurant/${id}`}>
                        <h2>Restaurant name: {name}</h2>
                        {averageRate !== null ?
                            <div>

                                <h2>Average rate:{ Number((averageRate).toFixed(1)) }</h2>
                                <ReactStars
                                    count={5}
                                    edit={false}
                                    size={32}
                                    value={Number((averageRate).toFixed(1))}
                                    half={true}
                                    activeColor="#ffd700"
                                />


                            </div>


                            : <h3>Be the first one to review </h3> }
                    </Link>





                    {role === "ADMIN" ? (
                        <div>
                            <Button variant="contained"  onClick={() => setShow(true)}>Delete</Button>
                            <Confirmation onClose={() => setShow(false)} show={show} childToParent={childToParent}
                                          message={"restaurant"}/>

                        </div>
                    ) : (<></>)}
                </div>
            </div>
        </div>

    );
};
export default RestaurantCard;