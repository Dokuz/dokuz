import {useHistory, useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import ReviewList from "../Review/ReviewList";
import ReviewCard from "../Review/ReviewCard";
import CreateReviews from "../Review/CreateReviews";
import Button from "@mui/material/Button";
import * as React from "react";

const RestaurantDetails = ({token, role}) => {
    const {id} = useParams()
    const [restaurant, setRestaurant] = useState()
    const [name, setName] = useState("");
    const [lastReview, setLastReview] = useState();
    const [maxRateReview, setMaxRateReview] = useState();
    const [minRateReview, setMinRateReview] = useState();
    const [reviews, setReviews] = useState([]);
    const history = useHistory();
    const [ReviewShow, setReviewsShow] = useState(false);
    const [added, setAdded] = useState(false);
    const [deleted, setDeleted] = useState(false);
    const [updated, setUpdated] = useState(false);
    const [show, setShow] = useState(false);


    const URL = 'http://localhost:9000/restaurant/' + id
    useEffect(() => {
        fetch(URL, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            }
        })
            .then(response => response.json())
            .then(body => {
                setRestaurant(body);
                setName(body["name"])
                setMaxRateReview(body["max_rate_review"])
                setMinRateReview(body["min_rate_review"])
                setLastReview(body["latest_rev"])
                setReviews(body["reviews"])
            })
            .catch(error => console.error('Error', error));

    }, [deleted, added, updated])

    const handleUpdate = () => {
        fetch('http://localhost:9000/admin/restaurant', {
            method: 'PATCH',
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            },
            body: JSON.stringify({
                id: id,
                name: name,
                reviews: reviews,
                max_rate_review: maxRateReview,
                mix_rate_review: minRateReview,
                latest_rev: lastReview,
            })
        }).then(() => {
            history.push('/');
        })
    }

    const onDeleted = () => {
        setDeleted(!deleted)
    }
    const onAdded = () => {
        setAdded(!added)
    }
    const onUpdated = () => {
        setUpdated(!updated)
    }
    return (

        <div className={"res-details"}>
            <div className={"modal-content"} onClick={e => e.stopPropagation()}>
                <div className="create">
                    <div className={"modal-header"}>

                        <Button variant="contained" sx={{mt: 3, mb: 2, mr: 3}}
                                onClick={() => setShow(true)}>Add New Review</Button>
                        <CreateReviews
                            token={token}
                            onClose={() => setShow(false)}
                            show={show}
                            addCallBack={onAdded}
                            restaurant_id={id}
                        />
                    </div>
                    <div className={"modal-body"}>
                        {restaurant ?
                            <div>
                                {role === "ADMIN" ?

                                    <div>
                                        <h2> Restaurant Name:</h2>
                                        <input
                                            type="text"
                                            required
                                            value={name}
                                            onChange={(e) => setName(e.target.value)}
                                        />
                                        <Button variant="contained" onClick={handleUpdate}>Save</Button>


                                    </div> :
                                    <div>
                                        <h2> Restaurant Name: {name} </h2>
                                    </div>

                                }


                                {maxRateReview ?
                                    <>
                                        <h3>Max Rated Review</h3> <ReviewCard
                                        key={maxRateReview["id"]}
                                        token={token}
                                        restaurant_id={id}
                                        id={maxRateReview["id"]}
                                        comment={maxRateReview["comment"]}
                                        date={maxRateReview["date"]}
                                        stars={maxRateReview["stars"]}
                                        deleteCallBack={onDeleted}
                                        updateCallBack={onUpdated}
                                        role={role}
                                        review={maxRateReview}
                                        username={maxRateReview["username"]}
                                    />

                                    </>

                                    : <></>}

                                {minRateReview ?
                                    <>
                                        <h3>Min Rated Review</h3>

                                        <ReviewCard
                                            key={minRateReview["id"]}
                                            token={token}
                                            restaurant_id={id}
                                            id={minRateReview["id"]}
                                            comment={minRateReview["comment"]}
                                            date={minRateReview["date"]}
                                            stars={minRateReview["stars"]}
                                            deleteCallBack={onDeleted}
                                            updateCallBack={onUpdated}
                                            role={role}
                                            review={minRateReview}
                                            username={minRateReview["username"]}
                                        />

                                    </>

                                    : <></>}

                                {lastReview ?
                                    <>
                                        <h3>Lasted Review</h3>
                                        <ReviewCard
                                            key={lastReview["id"]}
                                            token={token}
                                            restaurant_id={id}
                                            id={lastReview["id"]}
                                            comment={lastReview["comment"]}
                                            date={lastReview["date"]}
                                            stars={lastReview["stars"]}
                                            deleteCallBack={onDeleted}
                                            updateCallBack={onUpdated}
                                            role={role}
                                            review={lastReview}
                                            username={lastReview["username"]}
                                        />

                                    </>

                                    : <></>}

                                <div className={"modal-footer"}>
                                    {/*<button onClick={() => setReviewsShow(!ReviewShow)} > Show All reviews </button>*/}


                                    {
                                        role === "ADMIN" ?
                                            <div>
                                                <h3>All Reviews</h3>
                                                <ReviewList
                                                    token={token}
                                                    id={id}
                                                    ReviewOnClose={() => setReviewsShow(!ReviewShow)}
                                                    ReviewShow={ReviewShow}
                                                    added={added}
                                                    role={role}
                                                    update={updated}
                                                    deleteRes={deleted}
                                                    addRes={added}
                                                />

                                            </div>
                                            :
                                            <></>
                                    }


                                </div>
                            </div>


                            : <></>
                        }

                    </div>

                </div>


            </div>
        </div>

    );
}

export default RestaurantDetails;