import {useEffect, useState} from "react";
import ReactPaginate from 'react-paginate';
import CreateRestaurants from "./CreateRestaurants";
import RestaurantCard from "./RestaurantCard";
import * as React from "react";
import Button from "@mui/material/Button";



const RestaurantList = ({token, role}) => {
    const [show, setShow] = useState(false);
    const [restaurant, setRestaurants] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);
    const [pageCount, setPageCount] = useState(1);
    const [currentPage, setCurrentPage] = useState(1);
    const [deleted, setDeleted] = useState(false);
    //
    const URL = `http://0.0.0.0:9000/restaurant?page=${currentPage}`;
    const handlePageChange = (selectedObject) => {
        let a = selectedObject.selected;
        setCurrentPage(a + 1);
    };

    useEffect(() => {
        fetch(URL, {
            headers: {
                "Authorization": "Bearer " + token
            }
        })
            .then(response => response.json())
            .then(body => {
                setRestaurants([...body.restaurants]);
                setPageCount(body.page_count);
                setIsLoaded(true);
            })
            .catch(error => console.error('Error', error));
    }, [currentPage, deleted])


    const onDeleted = () => {
        setDeleted(!deleted)
    }

    return (

        <div className={"res-wrapper"}>
            {role === "ADMIN" ? (<div>

                <Button variant="contained" sx={{mt: 3, mb: 2}} onClick={() => setShow(true)}>Add New
                    Restaurant</Button>
                <CreateRestaurants token={token} onClose={() => setShow(false)} show={show}/>

            </div>) : (<div/>)}

            {isLoaded ? (
                restaurant.map((item) => {
                    return (
                        <RestaurantCard
                            key={item.id}
                            name={item.name}
                            id={item.id}
                            token={token}
                            deleteCallBack={onDeleted}
                            averageRate={item.avg_rate}
                            role={role}
                        />
                    );
                })
            ) : (
                <div/>
            )}
            {isLoaded ? (
                <ReactPaginate
                    pageCount={pageCount}
                    pageRange={5}
                    marginPagesDisplayed={5}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageChange}
                    containerClassName={'container'}
                    previousLinkClassName={'page'}
                    breakClassName={'page'}
                    nextLinkClassName={'page'}
                    pageClassName={'page'}
                    disabledClassName={'disabled'}
                    activeClassName={'active'}
                />
            ) : (
                <div>Nothing to display</div>
            )}

        </div>
    );
}

export default RestaurantList;