import {useState} from "react";
import {useHistory} from "react-router-dom";
import Button from "@mui/material/Button";
import * as React from "react";

const CreateRestaurants = (props) => {
    const [name, setName] = useState("");
    const history = useHistory();

    const  {token , onClose, show} = props
    if (!show) {
        return null
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        const restaurant = {name};
        fetch('http://localhost:9000/admin/restaurant', {
            method: 'POST',
            headers: {"Content-Type": "application/json", "Authorization": "Bearer " + token},
            body: JSON.stringify(restaurant)
        }).then(() => {
            // history.go(-1);
            history.push('/');
            onClose();
        }).catch((err)=>{
            console.log(err);
        })
    }

    return (
        <div className={"modal"} onClick={onClose}>
            <div className={"modal-content"} onClick={e => e.stopPropagation()}>
                <div className="create">
                    <div className={"modal-header"}>
                        <h2>Add a New Restaurant</h2>
                    </div>
                    <div className={"modal-body"}>
                        <div>

                            <label>Restaurant Name:</label>
                            <input
                                type="text"
                                required
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                            />

                            <div className={"modal-footer"}>
                                <Button variant="contained"  sx={{mt: 3, mb: 2, mr:3}} onClick={handleSubmit}>Add</Button>
                            </div>

                        </div>


                    </div>

                </div>


            </div>
        </div>

    )
        ;
}

export default CreateRestaurants;