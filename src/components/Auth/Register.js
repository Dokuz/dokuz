import React, { useState } from 'react';

const Register=({onClose, show}) =>{
    const [email, setEmail] = useState();
    const [fullName, setFullName] = useState();
    const [userName, setUserName] = useState();
    const [password, setPassword] = useState();

    // TO DO
    const handleSubmit = async e => {
        onClose();
        fetch('http://localhost:9000/auth/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email:email,
                password:password,
                fullname:fullName,
                username:userName
            })
        })
            .then(data => data.json())
        // TODO 409 handle


    }

    if(!show){
        return null
    }
    return(


        <div className={"modal"} onClick={onClose}>
            <div className={"modal-content"} onClick={e => e.stopPropagation()}>
                <div className="create">
                    <div className={"modal-header"}>
                        <h2> Create New Account </h2>
                    </div>
                    <div className={"modal-body"}>
                        <div className="login-wrapper">
                            <h1>Please Register</h1>

                            <div>

                                <label>
                                    <p>Full name:</p>
                                    <input type="text" onChange={e => setFullName(e.target.value)} />
                                </label>
                                <label>
                                    <p>User Name:</p>
                                    <input type="text" onChange={e => setUserName(e.target.value)} />
                                </label>
                                <label>
                                    <p>Email:</p>
                                    <input type="text" onChange={e => setEmail(e.target.value)} />
                                </label>
                                <label>
                                    <p>Password:</p>
                                    <input type="password" onChange={e => setPassword(e.target.value)} />
                                </label>
                                <div>
                                    <button type="submit" onClick={handleSubmit}>Register</button>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>


            </div>
        </div>






    )
}
export default Register;