import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Register from "./Register";

async function loginUser(credentials) {
    return fetch('http://localhost:9000/auth/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(credentials)
    })
        .then(data => data.json())
    // TODO 401 handle

}


export default function Login({setToken, setRole}) {
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [show, setShow] = useState(false)


    // TO DO
    const handleSubmit = async e => {
        e.preventDefault();
        const token = await loginUser({
            email,
            password
        });
        setToken(token["result"]["token"], token["result"]["role"]);
    }

    return (

        <div className="login-wrapper"  style={{"text-align": "center"}}>
            <div>
                <h1>Please Log In</h1>
                <label>
                    <p>Email</p>
                    <input type="text" onChange={e => setEmail(e.target.value)}/>
                </label>
                <label>
                    <p>Password</p>
                    <input type="password" onChange={e => setPassword(e.target.value)}/>
                </label>
                <div>
                    <button type="submit" onClick={handleSubmit}>Login</button>
                </div>
            </div>

            <button onClick={() => setShow(true)}>Create New Account</button>
            <Register onClose={() => setShow(false)} show={show}/>


        </div>
    )
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired,
};