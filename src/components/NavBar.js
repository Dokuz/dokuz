import {useHistory} from "react-router-dom";
import Button from "@mui/material/Button";
import * as React from "react";

const Navbar = ({setToken, role}) => {
    const history = useHistory();

    const logOut = () => {
        setToken(undefined, undefined);
        let path = `/`;
        history.push(path);
    }

    const changePath = (path) => {
        history.push(path);
    }

    return (
                <nav className="navbar">
                    <div className="links">
                        <Button onClick={() => changePath("/restaurant")} variant="contained"
                                sx={{mt: 3, mb: 2, mr:3}}>Restaurants</Button>

                        {
                            role === "ADMIN" ?
                                <div>
                                    <Button onClick={() => changePath("/user")} variant="contained"
                                            sx={{mt: 3, mb: 2, mr:3}}>Users</Button>
                                </div> :

                                <></>
                        }


                        {/*<Link to="/user" style={{*/}
                        {/*    color: 'black',*/}
                        {/*    backgroundColor: '#f1356d',*/}
                        {/*    borderRadius: '8px'*/}
                        {/*}}>Users</Link>*/}
                        <Button variant="contained" sx={{mt: 3, mb: 2}} onClick={logOut}>Logout</Button>

                    </div>
                </nav>
    );
}

export default Navbar;