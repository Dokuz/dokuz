import {useState} from 'react';

export default function useToken() {
    const getToken = () => {
        const tokenString = localStorage.getItem('token');
        const roleString = localStorage.getItem('role');
        if (!tokenString)
            return undefined;
        return {token:tokenString, role:roleString}
    };

    const saveToken = (token, role) => {
        if(token === undefined || role === undefined){
            localStorage.clear();
            setToken(undefined);
        }else {
            localStorage.setItem('token', token);
            localStorage.setItem('role', role);
            setToken({token:token, role:role});
        }
    };

    const [token, setToken] = useState(getToken());

    return {
        setToken: saveToken,
        token,
    }
}