import Confirmation from "../Confirmation";
import {useState} from "react";
import ReviewDetail from "./ReviewDetail";
import Button from "@mui/material/Button";
import * as React from "react";
import Grid from "@mui/material/Grid";
import RateReviewIcon from '@material-ui/icons/RateReview';
const ReviewCard = ({
                        restaurant_id,
                        id,
                        token,
                        comment,
                        stars,
                        deleteCallBack,
                        updateCallBack,
                        username,
                        role,
                        review,
                        date
                    }) => {
    const [show, setShow] = useState(false);
    const [showDetail, setShowDetail] = useState(false);


    const handleDelete = () => {
        fetch('http://localhost:9000/admin/review', {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            },
            body: JSON.stringify(
                {
                    restaurant_id: restaurant_id,
                    review_id: id
                }
            )
        }).then(() => {
            deleteCallBack()
        })
    }
    const childToParent = (childData) => {
        if (childData) {
            handleDelete()

        }
        setShow(false);
    }
    return (
        <Grid container className={"res-list"}   direction="row"
              alignItems="center"
              justifyContent="center">
            <Grid item xs={2}>
                <RateReviewIcon fontSize={"large"}/>
            </Grid>
            <Grid item xs={10}>
                <div>
                    <div className="res-preview">
                        <div>
                            <h2>Rate: {stars}</h2>
                            <h2>Comment:{comment}</h2>
                            <h2>Comment Owner:{username} </h2>
                            <h2>Review date:{(new Date(date * 1000)).toLocaleString()} </h2>

                            {
                                role === "ADMIN" ?
                                    <div>
                                        <Button variant="contained" style={{margin: 'left'}}
                                                onClick={() => setShow(true)}>Delete</Button>
                                        <Confirmation
                                            onClose={() => setShow(false)}
                                            show={show}
                                            childToParent={childToParent}
                                            message={"review"}/>

                                        <Button variant="contained"
                                                onClick={() => setShowDetail(true)}>Update</Button>
                                        <ReviewDetail
                                            token={token}
                                            onClose={() => setShowDetail(false)}
                                            showDetail={showDetail}
                                            restaurant_id={restaurant_id}
                                            reviewComment={comment}
                                            reviewStars={stars}
                                            id={id}
                                            updateCallBack={updateCallBack}
                                        />
                                    </div>
                                    :
                                    <></>
                            }


                        </div>
                    </div>
                </div>
            </Grid>
        </Grid>

    );
};

export default ReviewCard;