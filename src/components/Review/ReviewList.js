import {useEffect, useState} from "react";
import ReactPaginate from "react-paginate";
import CreateReviews from "./CreateReviews";
import ReviewCard from "./ReviewCard";


const ReviewList = ({token, id, role, added,update}) => {
    const [show, setShow] = useState(false);
    const [reviews, setReviews] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);
    const [pageCount, setPageCount] = useState(1);
    const [currentPage, setCurrentPage] = useState(1);
    const [deleted, setDeleted] = useState(false);
    // const [added, setAdded] = useState(false);
    const [updated, setUpdated] = useState(false);


    const URL = `http://0.0.0.0:9000/admin/restaurant/${id}/review?page=${currentPage}`;

    const handlePageChange = (selectedObject) => {
        let a = selectedObject.selected
        setCurrentPage(a + 1);
    };
    useEffect(() => {
        fetch(URL, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            }
        })
            .then(response => response.json())
            .then(body => {
                console.log("body", body)
                setReviews([...body['reviews']]);
                setPageCount(body.page_count);
                setIsLoaded(true);
            })
            .catch(error => console.error('Error', error));
    }, [currentPage, deleted, added,updated,update])

    // if(!ReviewShow){
    //     return null
    // }
    console.log("pageCount", pageCount)
    const onDeleted = () => {
        setDeleted(!deleted)
    }
    // const onAdded = () => {
    //    setAdded(!added)
    // }
    const onUpdated = () => {
        setUpdated(!updated)
    }


    return (

        <>
            {isLoaded ? (
                reviews.map((item) => {
                    return (
                        <ReviewCard
                            key={item.id}
                            stars={item.stars}
                            comment={item.comment}
                            id={item.id}
                            deleteCallBack={onDeleted}
                            updateCallBack={onUpdated}
                            restaurant_id={id}
                            token={token}
                            role={role}
                            review={item}
                            username={item.username}
                            date={item.date}
                        />
                    );
                })
            ) : (
                <div/>
            )}
            {isLoaded ? (
                <ReactPaginate
                    pageCount={pageCount}
                    pageRange={5}
                    marginPagesDisplayed={5}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageChange}
                    containerClassName={'container'}
                    previousLinkClassName={'page'}
                    breakClassName={'page'}
                    nextLinkClassName={'page'}
                    pageClassName={'page'}
                    disabledClassNae={'disabled'}
                    activeClassName={'active'}
                />
            ) : (
                <div>Nothing to display</div>
            )}


        </>
    );
}

export default ReviewList;