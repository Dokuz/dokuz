import {useState} from "react";

import ReactStars from "react-rating-stars-component";
import Button from "@mui/material/Button";

const CreateReviews = ({token, onClose, show, addCallBack, restaurant_id}) => {
    const [stars, setStars] = useState(0);
    const [comment, setComment] = useState('');
    if (!show) {
        return null
    }
    const ratingChanged = (newRating) => {
        setStars(newRating)
    };
    const handleSubmit = (e) => {
        e.preventDefault();
        onClose();
        fetch('http://localhost:9000/review', {
            method: 'POST',
            headers: {"Content-Type": "application/json", "Authorization": "Bearer " + token},
            body: JSON.stringify(
                {
                    restaurant_id: restaurant_id,
                    review: {
                        comment: comment,
                        stars: stars
                    }
                }
            )
        }).then(() => {
            addCallBack()
        })
        setStars(0);
        setComment('');
    }

    return (
        <div className={"modal"} onClick={onClose}>
            <div className={"modal-content"} onClick={e => e.stopPropagation()}>
                <div className="create">
                    <div className={"modal-header"}>
                        <h2>Add a New Review</h2>
                    </div>
                    <div className={"modal-body"}>
                        <div>
                            <label>Rate Restaurant:</label>
                            <ReactStars
                                count={5}
                                onChange={ratingChanged}
                                size={32}
                                value={stars}
                                half={true}
                                activeColor="#ffd700"
                            />
                            <label>Comment:</label>
                            <textarea
                                required
                                value={comment}
                                onChange={(e) => setComment(e.target.value)}
                            />

                            <div className={"modal-footer"}>
                                {/*<button onClick={handleSubmit} className={'button'}>Add</button>*/}
                                <Button variant="contained"  onClick={handleSubmit}>Add</Button>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    );
}

export default CreateReviews;