import {useState} from "react";
import ReactStars from "react-rating-stars-component";
import Button from "@mui/material/Button";
import * as React from "react";

const ReviewDetail = ({token, onClose, showDetail, restaurant_id,reviewComment,reviewStars,id,updateCallBack}) => {
    const [stars, setStars] = useState(reviewStars);
    const [comment, setComment] = useState(reviewComment);
    const ratingChanged = (newRating) => {
        setStars(newRating)
    };

    if (!showDetail) {
        return null
    }
    const handleUpdate = (e) => {
        e.preventDefault();
        onClose();
        fetch('http://localhost:9000/admin/review', {
            method: 'PATCH',
            headers: {"Content-Type": "application/json", "Authorization": "Bearer " + token},
            body: JSON.stringify(
                {
                    restaurant_id: restaurant_id,
                    review: {
                        id:id,
                        comment: comment,
                        stars: stars
                    }


                }
            )
        }).then(() => {
            updateCallBack()
        })
    }

    return (
        <div className={"modal"} onClick={onClose}>
            <div className={"modal-content"} onClick={e => e.stopPropagation()}>
                <div className="create">
                    <div className={"modal-header"}>
                        <h2>Review Details</h2>
                    </div>
                    <div className={"modal-body"}>
                        <div>
                            <label>Restaurant Rate:</label>
                            <ReactStars
                                style={{"justify-content":"center"}}
                                count={5}
                                onChange={ratingChanged}
                                size={32}
                                value={stars}
                                activeColor="#ffd700"
                            />
                            <label>Comment:</label>
                            <textarea
                                required
                                value={comment}
                                onChange={(e) => setComment(e.target.value)}
                            />

                            <div className={"modal-footer"}>
                                {/*<button onClick={handleUpdate} className={'button'}>Update</button>*/}
                                <Button variant="contained"  onClick={handleUpdate}>Update</Button>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    );
}

export default ReviewDetail;