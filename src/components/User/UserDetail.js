import { useHistory, useParams } from "react-router-dom";
import {useEffect, useState} from "react";
import Button from "@mui/material/Button";
import * as React from "react";

const UserDetail = ({token}) => {
    const {id} = useParams()
    const [user, setUser]=useState()
    const URL ='http://localhost:9000/admin/user/' + id
    const history = useHistory();

    const [role, setRole] = useState("");
    const [password, setPassword] = useState("");
    const [username, setName] = useState("");
    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");

    useEffect(()=>{
        fetch(URL, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            }
        })
            .then(response => response.json())
            .then(body => {
                setUser(body);
                setName(body["username"])
                setFullName(body["fullName"])
                setEmail(body["email"])
                setPassword(body["password"])
                setRole(body["role"])
            })
            .catch(error => console.error('Error', error));

    },[])

    const handleUpdate = () => {
        fetch('http://localhost:9000/admin/user', {
            method: 'PATCH',
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token,
            },
            body:JSON.stringify({
                user_id:id,
                fullname:fullName,
                username:username,
                email:email,
                password:password,
                role:role
            })
        }).then(() => {
            history.push('/user');
        })
    }

    return (

        <div className={"res-details"}>
            <div className={"modal-content"} onClick={e => e.stopPropagation()}>
                <div className="create">
                    {/*<div className={"modal-header"}>*/}
                    {/*    <button> onSubmit={setShow(!show)} className={'button'} >Add User</button>*/}
                    {/*</div>*/}
                    <div className={"modal-body"}>
                        {user?
                            <div>
                                <label>Full Name:</label>
                                <input
                                    type="text"
                                    required
                                    value={fullName}
                                    onChange={(e) => setFullName(e.target.value)}
                                />
                                <label>User Name:</label>
                                <input
                                    type="text"
                                    required
                                    value={username}
                                    onChange={(e) => setName(e.target.value)}
                                />
                                <label>Email:</label>
                                <input type="text" value={email} onChange={e => setEmail(e.target.value)}/>

                                <label>Password:
                                    <input type="password" value={password} onChange={e => setPassword(e.target.value)}/>
                                </label>

                                <label>Role:
                                    <select value={role} onChange={e => setRole(e.target.value)}>
                                        <option value="select">select</option>
                                        <option value="ADMIN">admin</option>
                                        <option value="USER">user</option>
                                    </select>
                                </label>

                                <div className={"modal-footer"}>
                                    <Button variant="contained"  onClick={handleUpdate}>Save</Button>
                                </div>
                            </div>


                            :<></>}

                    </div>

                </div>


            </div>
        </div>
    );
}

export default UserDetail;