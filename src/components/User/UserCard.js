import {Link} from "react-router-dom";
import Confirmation from "../Confirmation";
import {useState} from "react";
import Button from "@mui/material/Button";
import * as React from "react";


const UserCard = ({username, email, id,role,token,deleteCallBack,fullname}) => {
    const [show, setShow] = useState(false);
    const handleDelete = () => {
        fetch('http://localhost:9000/admin/user/'+id, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            },
        }).then(() => {
            // console.log("delete child");
            deleteCallBack()
            // history.push('/');
        })
    }
    const childToParent = (childData) => {
        if(childData){
            handleDelete()

        }
        setShow(false);
    }


    return (
        <div className={"res-list"}>
            <div style={{padding: "20"}}>
                <div className="res-preview">
                    <div>
                        <Link to={`/user/${id}`}>
                            <h2>Username: {username}</h2>
                            <h2>Email:{email}</h2>
                            <h2>Role:{role}</h2>
                            <h2>Full Name:{fullname}</h2>
                        </Link>
                        <Button variant="contained"  sx={{mt: 3, mb: 2, mr:3}} onClick={() => setShow(true)}>Delete</Button>
                        <Confirmation onClose={() => setShow(false)} show={show} childToParent={childToParent} message={"user"}/>
                    </div>
                </div>
            </div>
        </div>

    );
};
export default UserCard;