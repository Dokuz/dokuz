import {useState} from "react";
import Button from "@mui/material/Button";
import * as React from "react";

const CreateUser = (props) => {
    const [username, setName] = useState("");
    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState();
    const [role, setRole] = useState('USER');
    const [password, setPassword] = useState();
    const [status, setStatus] = useState();
    const {token, onClose, show} = props
    const [showError, setShowError] = useState(false);

    if (!show) {
        return null
    }


    const handleSubmit = (e) => {
        e.preventDefault();

        const users = {fullName, email, username, password, role};
        fetch('http://localhost:9000/admin/user', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            },
            body: JSON.stringify(users)
        }).then((res) => {
            props.addCallBack()
            if (res.status === 409) {
                setShowError(true);
                setStatus("Email already exists!");
            } else {
                setShowError(false);
                props.onClose()
            }

        })

    }

    return (
        <div className={"modal"} onClick={onClose}>
            <div className={"modal-content"} onClick={e => e.stopPropagation()}>
                <div className="create">
                    <div className={"modal-header"}>
                        <h2>Add a New User</h2>
                    </div>
                    <div className={"modal-body"}>

                        <div>
                            <label>Full Name:</label>
                            <input
                                type="text"
                                required
                                value={fullName}
                                onChange={(e) => setFullName(e.target.value)}
                            />
                            <label>User Name:</label>
                            <input
                                type="text"
                                required
                                value={username}
                                onChange={(e) => setName(e.target.value)}
                            />
                            <label>Email:</label>
                            <input type="text" onChange={e => setEmail(e.target.value)}/>

                            <label>Password:
                                <input type="password" onChange={e => setPassword(e.target.value)}/>
                            </label>

                            <label>Role:
                                <select value={role} onChange={e => setRole(e.target.value)}>
                                    <option value="ADMIN">admin</option>
                                    <option value="USER">user</option>
                                </select>
                            </label>
                            {showError ?
                                <p style={{color: "red"}}> {status} </p> : <></>
                            }
                            <div className={"modal-footer"}>
                                <Button variant="contained" sx={{mt: 3, mb: 2, mr: 3}}
                                        onClick={handleSubmit}>Add</Button>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>

    );
}

export default CreateUser;