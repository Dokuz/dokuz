import ReactPaginate from 'react-paginate';
import {useEffect, useState} from "react";
import CreateUser from "./CreateUser";
import UserCard from "./UserCard";
import Button from "@mui/material/Button";
import * as React from "react";


const UserList = ({token}) => {
    const [show,setShow]=useState(false);
    const [users, setUsers] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);
    const [pageCount, setPageCount] = useState(1);
    const [currentPage, setCurrentPage] = useState(1);
    const [deleted, setDeleted] = useState(false);
    const [added, setAdded] = useState(false);

    const URL = `http://0.0.0.0:9000/admin/user?page=${currentPage}`;
    const handlePageChange = (selectedObject) => {
        let a = selectedObject.selected
        setCurrentPage(a+1);
    };


    useEffect(()=>{
        fetch(URL, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            }
        })
            .then(response => response.json())
            .then(body => {
                // console.log("body", body)
                setUsers([...body['users']]);
                setPageCount(body.page_count);
                setIsLoaded(true);
            })
            .catch(error => console.error('Error', error));

    },[currentPage,deleted,added])



    const onDeleted = ()=>{
        setDeleted(!deleted)
    }
    const onAdded = ()=>{
        setAdded(!added)
    }


    return (
        <>
            <Button variant="contained"  sx={{mt: 3, mb: 2, mr:3}} onClick={() => setShow(true)}>Add New User</Button>

            <CreateUser token={token} onClose={() => setShow(false)} show={show} addCallBack={onAdded}/>


            {isLoaded ? (
                users.map((item) => {
                    return (
                        <UserCard
                            key={item.id}
                            email={item.email}
                            username={item.username}
                            fullname={item.fullname}
                            id={item.id}
                            role={item.role}
                            token={token}
                            deleteCallBack={onDeleted}
                        />
                    );
                })
            ) : (
                <div/>
            )}
            {isLoaded ? (
                <ReactPaginate
                    pageCount={pageCount}
                    pageRange={5}
                    marginPagesDisplayed={5}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageChange}
                    containerClassName={'container'}
                    previousLinkClassName={'page'}
                    breakClassName={'page'}
                    nextLinkClassName={'page'}
                    pageClassName={'page'}
                    disabledClassNae={'disabled'}
                    activeClassName={'active'}
                />
            ) : (
                <div></div>
            )}


        </>


    );
}

export default UserList;