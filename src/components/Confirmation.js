import Button from "@mui/material/Button";
import * as React from "react";

const Confirmation = (props) => {

    const  {onClose, show, childToParent,message} = props


    if(!show){
        return null
    }

    return (
        <div className={"modal"} onClick={onClose}>
            <div className={"modal-content"} onClick={e => e.stopPropagation()}>
                <div className="create">
                    <div className={"modal-header"}>
                        <h2> Are you sure you want to delete {message} </h2>
                    </div>
                    <div className={"modal-body"}>
                        <Button variant="contained"  sx={{mt: 3, mb: 2, mr:3}} onClick={() => childToParent(0)}>Cancel</Button>
                        <Button variant="contained"  sx={{mt: 3, mb: 2, mr:3}} onClick={() => childToParent(1)}>Delete</Button>
                    </div>

                </div>


            </div>
        </div>

    )
        ;
}

export default Confirmation;