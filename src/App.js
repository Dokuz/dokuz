import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import './App.css';
import Login from "./components/Auth/Login2";
import useToken from "./components/CustomHooks/useToken";
import Navbar from "./components/NavBar";
import RestaurantDetails from "./components/Restaurant/RestaurantDetails";
import UserList from "./components/User/UserList";
import RestaurantList from "./components/Restaurant/RestaurantList";
import UserDetail from "./components/User/UserDetail";
import CssBaseline from "@mui/material/CssBaseline";
import * as React from "react";
import {ThemeProvider} from "@emotion/react";
import {createTheme} from "@mui/material/styles";
import Container from "@mui/material/Container";

const theme = createTheme();


function App() {
    const {token, setToken} = useToken();

    if (!token) {
        return (
            <Login setToken={setToken}/>
        )
    }

    return (

        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline/>

                <div className="wrapper" align={"center"}>
            <BrowserRouter>
                <Navbar setToken={setToken} role={token.role}/>
                {/*{token.role === "ADMIN" &&*/}
                {/*    <Navbar setToken={setToken} role={token.role}/>*/}
                {/*}*/}
                <Switch>
                    {/*todo*/}

                    <Route exact path="/restaurant">
                        <RestaurantList token={token.token} role={token.role}/>
                    </Route>

                    <Route exact path="/">
                        <RestaurantList token={token.token} role={token.role}/>
                    </Route>

                    <Route path="/restaurant/:id">
                        <RestaurantDetails token={token.token} role={token.role}/>
                    </Route>

                    {/*<Route exact path="/user">*/}
                    {/*    <Register/>*/}
                    {/*</Route>*/}

                    <Route
                        exact
                        path="/user"
                        render={(props) =>
                            token.role==="ADMIN" ? (
                                <UserList token={token.token}/>
                            ) : (
                                <Redirect to="/"/>
                            )
                        }
                    />

                    <Route
                        exact
                        path="/restaurant:id"
                        render={(props) =>

                            token.role==="ADMIN" ? (

                                <RestaurantDetails token={token.token}/>
                            ) : (
                                <Redirect to="/"/>
                            )
                        }
                    />
                    <Route
                        exact
                        path="/user/:id"
                        render={(props) =>
                            token.role==="ADMIN" ? (
                                <UserDetail token={token.token}/>
                            ) : (
                                <Redirect to="/"/>
                            )
                        }
                    />

                </Switch>
            </BrowserRouter>
        </div>

            </Container>
        </ThemeProvider>

    );
}

export default App;